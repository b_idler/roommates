﻿using System.Security.Principal;
using System.Web.Mvc;
using Roommates.Common.Services;
using Roommates.DataLayer.Model;
using log4net;

namespace Roommates.WebUI.Controllers
{
    public class BaseController : Controller
    {
        public RoommatesEntities RoommatesEntities = new RoommatesEntities();
        public IEventLogger Logger = new EventLogger(LogManager.GetLogger(typeof(BaseController)));
    }
}
