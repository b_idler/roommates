﻿using System;
using System.Text;
using System.Web.Mvc;
using System.Net;
using System.Web.Script.Serialization;

namespace Roommates.WebUI.Controllers
{
    public class VkAnswer
    {
        public string access_token;
        public string expires_in;
        public string user_id;
    }

    public class VKController : Controller
    {
        private const string AppID = "3712308";
        private const string AppSecret = "EeqmIZ6mGzTLyAXwKs8p";

        private const string Settings = "";

        public ActionResult GetVkAccessCode(string redirectUrlBase64, string code)
        {
            string redirectUrl = Encoding.UTF8.GetString(Convert.FromBase64String(redirectUrlBase64));

            var urlToVkAccessCode = this.Url.Action("GetVkAccessCode", "VK", null, Request.Url.Scheme);
            string url = string.Format("https://oauth.vk.com/access_token?client_id={0}&client_secret={1}&code={2}&redirect_uri={3}?redirectUrlBase64={4}", AppID, AppSecret, code, urlToVkAccessCode, redirectUrlBase64);

            var webClient = new WebClient();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var result = serializer.Deserialize<VkAnswer>(webClient.DownloadString(url));

            Session["access_token"] = result.access_token;
            Session["user_id"] = result.user_id;

            return Redirect(redirectUrl);
        }

        [HttpGet]
        public ActionResult LogOut()
        {
            Session["access_token"] = "";
            Session["user_id"] = "";
            return null;
        }
    }
}
