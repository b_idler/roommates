﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Roommates.Common.Consts;
using Roommates.DataLayer.Model;
using Roommates.WebUI.Models.ConrolsModels;
using Roommates.WebUI.Models.TicketModels;

namespace Roommates.WebUI.Controllers
{
    public class RegistrationController : BaseController
    {
        public ActionResult Index()
        {
            TicketModel ticket = new TicketModel()
                {
                    SliderPrice = new SliderModel()
                        {
                            PriceMax = 15000,
                            PriceMin = 5000
                        },
                        Price = 10000,
                        NeighbourSex = Sex.NotImportant,
                        PersonsCount = PersonCount.NotImportant,
                        TicketTypes = TicketTypes.Human
                };
            return View(ticket);
        }

        public ActionResult RegisterNewTicket(TicketModel model)
        {
            var ticket = new Ticket();
            try
            {
                model.OnSetModel(ticket);
                var housePhotos = Request.Files;
                var photos = new Collection<Photo>();
                foreach (var housePhotokey in housePhotos.AllKeys)
                {
                    if (!String.IsNullOrWhiteSpace(housePhotos[housePhotokey].FileName))
                    {
                        var photo = new Photo
                            {
                                Photo1 = System.IO.File.ReadAllBytes(housePhotos[housePhotokey].FileName)
                            };
                        photos.Add(photo);
                    }

                }
                var vkId = (string) Session["user_id"];
                Person person = RoommatesEntities.People.FirstOrDefault(q => q.VkID == vkId);
                if (person == null)
                {
                    person = new Person()
                        {
                            VkID = (string)Session["user_id"]
                        };
                    RoommatesEntities.People.Add(person);
                }
                ticket.Person = person;
                //ticket.Photos = photos;
                RoommatesEntities.Tickets.Add(ticket);
                RoommatesEntities.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Write(String.Format("{0}; {1}", e.Message, e.InnerException), EventTypes.Error);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}