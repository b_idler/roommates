﻿using System.Web.Mvc;
using Roommates.Common.Consts;
using Roommates.WebUI.Models.ConrolsModels;
using Roommates.WebUI.Models.TicketModels;

namespace Roommates.WebUI.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            //List<TicketModel> allTickets = RoommatesEntities.Tickets.ToList().Select(item => new TicketModel(item)).ToList();
            var searchModel = new TicketsSearchModel()
                {
                    SliderModel = new SliderModel()
                        {
                            PriceMin = 5000,
                            PriceMax = 15000
                        },
                    NeighbourSex = Sex.NotImportant,
                    PersonsCount = PersonCount.NotImportant,
                    Smoking = false,
                    Pets = false,
                    TicketTypes = TicketTypesSearch.NotImportant
                };
            return View(searchModel);
        }
    }
}
