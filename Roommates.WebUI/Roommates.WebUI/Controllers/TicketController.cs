﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Roommates.Common.Consts;
using Roommates.DataLayer.Model;
using Roommates.WebUI.Models.ConrolsModels;
using Roommates.WebUI.Models.StandartModels;
using Roommates.WebUI.Models.TicketModels;

namespace Roommates.WebUI.Controllers
{
    public class TicketController : BaseController
    {
        /// <summary>
        /// Тестовый метод - берет все тикеты с указанными параметрами и вызывает view, которая рисует табличку с ними
        /// </summary>
        public ActionResult Index()
        {
            JsonResult jsonResult = GetTickets(new TicketsSearchModel
                {
                    LeftUpCorner = new PointDouble(-1000, -1000),
                    RightDownCorner = new PointDouble(1000, 1000),
                    SliderModel = new SliderModel
                        {
                            PriceMin = 0,
                            PriceMax = 100000
                        },
                    Smoking = false,
                    Pets = false,
                    NeighbourSex = Sex.Male,
                    PersonsCount = PersonCount.Few
                });
            List<Ticket> tickets = jsonResult.Data as List<Ticket>;
            return View(tickets);
        }

        /// <summary>
        /// Тестовый метод - добавляем "случайный" тикет в базу, а потом вызываем тестовый метод Index
        /// </summary>
        public ActionResult AddRandomTicket()
        {
            Random rand = new Random(DateTime.Now.Millisecond);
            
            int minPrice = rand.Next(10000);
            AddTicket(new PointDouble(rand.NextDouble(), rand.NextDouble()),
                      new PointDouble(rand.NextDouble(), rand.NextDouble()), minPrice, minPrice + rand.Next(10000),
                      false, false, true, (int) Sex.Male, (int) PersonCount.Few, 1);
            return RedirectToAction("Index");
        }

        public JsonResult GetAllTickets()
        {
            List<TicketModel> tickets = RoommatesEntities.Tickets.Include("Person").ToList().Select(item => new TicketModel(item)).ToList();
            return Json(tickets);
        }

        /// <summary>
        /// Рабочий метод - получает набор тикетов, подходящих под указанные параметры.
        /// Возвращает Json
        /// </summary>
        public JsonResult GetTickets(TicketsSearchModel searchModel)
        {
            Logger.Write(String.Format("Call getTickets whith priceMax: {0}, priceMin: {1}, [{2}, {3}] [{4}, {5}]",
                                       searchModel.SliderModel.PriceMax, searchModel.SliderModel.PriceMin,
                                       searchModel.LeftUpCorner.X, searchModel.LeftUpCorner.Y,
                                       searchModel.RightDownCorner.X, searchModel.RightDownCorner.Y));
            IQueryable<Ticket> queryableTickets = RoommatesEntities.Tickets.Include("Person");
            queryableTickets = queryableTickets.Where(ticket => ticket.PriceMax >= searchModel.SliderModel.PriceMin && ticket.PriceMin <= searchModel.SliderModel.PriceMax);
            queryableTickets = queryableTickets.Where(ticket => (!searchModel.Smoking || ticket.Smoking == searchModel.Smoking) && 
                                                                (!searchModel.Pets || ticket.Pets == searchModel.Pets) &&
                                                                (searchModel.PersonsCount == PersonCount.NotImportant || (int) searchModel.PersonsCount == ticket.PersonsCount));
            queryableTickets =
                queryableTickets.Where(ticket => (searchModel.TicketTypes == TicketTypesSearch.NotImportant ||
                                                 (searchModel.TicketTypes == TicketTypesSearch.Appartment && ticket.WithHouse.HasValue && ticket.WithHouse.Value) ||
                                                 (searchModel.TicketTypes == TicketTypesSearch.Human && ticket.WithHouse.HasValue && !ticket.WithHouse.Value)));
            queryableTickets = queryableTickets.Where(ticket => searchModel.NeighbourSex == Sex.NotImportant || (byte)searchModel.NeighbourSex == ticket.Sex);
            List<Ticket> ticketTables = queryableTickets.ToList();
            ticketTables = ticketTables.Where(ticket => IsTicketInSelect(ticket, searchModel.LeftUpCorner, searchModel.RightDownCorner)).ToList();
            List<TicketModel> ticketModels = ticketTables.Select(q => new TicketModel(q)).ToList();
            return Json(ticketModels);
        }

        /// <summary>
        /// Рабочий метод - добавляет тикет в базу. На данный момент, не добавляет фото.
        /// </summary>
        public void AddTicket(PointDouble leftUpCorner, PointDouble rightDownCorner, int priceMin, int priceMax, bool smoking, bool pets, bool sex, int neighbourSex, int personCount, int personId)
        {
            PointDouble center = new PointDouble((leftUpCorner.X + rightDownCorner.X) / 2, (leftUpCorner.Y + rightDownCorner.Y) / 2);
            leftUpCorner.X /= 2;
            leftUpCorner.Y /= 2;
            rightDownCorner.X /= 2;
            rightDownCorner.Y /= 2;
            double radius = leftUpCorner.GetDistance(rightDownCorner);
            Ticket ticket = new Ticket
                {
                    PersonID = personId,
                    Sex = sex ? (byte)1 : (byte)0,
                    XCoord = center.X,
                    YCoord = center.Y,
                    Radius = radius,
                    Pets = pets,
                    Smoking = smoking,
                    PersonsCount = personCount,
                    NeighbourSex = (byte) neighbourSex,
                    PriceMin = priceMin,
                    PriceMax = priceMax
                };
            RoommatesEntities.Tickets.Add(ticket);
            try
            {
                RoommatesEntities.SaveChanges();
            }
            catch(Exception e)
            {
                Logger.Write(String.Format("{0}; {1}", e.Message, e.InnerException), EventTypes.Error);
            }
        }
        
        #region Private Methods

        /// <summary>
        /// На данный момент смотрим 
        ///     1) лежит ли центр тикета в выделенной области
        ///     2) касаются ли окружности тикета и выделенной области (да, выделенная область во втором случае, рассматривается, как окружность)
        /// </summary>
        private bool IsTicketInSelect(Ticket ticket, PointDouble leftUpCorner, PointDouble rightDownCorner)
        {
            double x = ticket.XCoord;
            double y = ticket.YCoord;
            //check ticket center in selected area
            if (x >= leftUpCorner.X && x <= rightDownCorner.X && y >= leftUpCorner.Y && y <= rightDownCorner.Y)
            {
                return true;
            }
            return false;

            /*leftUpCorner.X /= 2;
            rightDownCorner.X /= 2;
            leftUpCorner.Y /= 2;
            rightDownCorner.Y /= 2;
            PointDouble centerSelected = new PointDouble(leftUpCorner.X + rightDownCorner.X, leftUpCorner.Y + rightDownCorner.Y);
            double radiusSelected = leftUpCorner.GetDistance(rightDownCorner);
            double fullRadius = radiusSelected + ticket.Radius;
            return (centerSelected.GetDistance(new PointDouble(x, y)) < fullRadius);*/
        }

        #endregion
    }
}
