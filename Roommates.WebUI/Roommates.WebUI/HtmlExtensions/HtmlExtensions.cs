﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Roommates.Common.Attributes;

namespace Roommates.WebUI.HtmlExtensions
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString RadioButtonForEnum<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, Dictionary<string, object> labelAttrs = null
            )
        {
            ModelMetadata metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var sb = new StringBuilder();

            foreach (var memInfo in metaData.ModelType.GetFields())
            {
                var attribute =
                    (DescriptionAttribute) memInfo.GetCustomAttribute(typeof (DescriptionAttribute), false);
                if (attribute == null)
                    continue;
                string description = attribute.Description;

                string id = String.Format(
                    "{0}_{1}",
                    metaData.PropertyName,
                    memInfo.Name
                    );

                string radio = htmlHelper.RadioButtonFor(expression, memInfo.Name, new {id}).ToHtmlString();
                sb.AppendFormat(
                    "<label class=\"radio\" for=\"{0}\">{2}{1}</label>",
                    id,
                    HttpUtility.HtmlEncode(description),
                    radio
                    );
            }
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString SliderFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
                                                                 Expression<Func<TModel, TProperty>> expression)
        {
            ModelMetadata sliderMetaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            PropertyInfo sliderProperty = htmlHelper.ViewData.Model.GetType().GetProperty(sliderMetaData.PropertyName);

            var stepAttr = (StepAttribute)Attribute.GetCustomAttribute(sliderProperty, typeof(StepAttribute));
            var step = stepAttr.StepValue;

            var sliderRangeAttr = (SliderRangeAttribute)Attribute.GetCustomAttribute(sliderProperty, typeof(SliderRangeAttribute));
            var sliderMin = sliderRangeAttr.MinValue;
            var sliderMax = sliderRangeAttr.MaxValue;

            string sliderHtml = htmlHelper.EditorFor(expression, new { step, sliderMin, sliderMax }).ToHtmlString();
            return MvcHtmlString.Create(sliderHtml);
        }
    }
}