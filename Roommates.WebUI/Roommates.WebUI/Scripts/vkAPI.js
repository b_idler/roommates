﻿var uids = "";
var fields = {};
var vkInfoPrefix = "vk_info_";

function callbackFunc(result) {
    for (var i in result.response) {
        var block = result.response[i];

        var field_keys = Object.keys(fields);
        for (var k in field_keys) {
            var elements = $("." + vkInfoPrefix + block.uid).find("[class~='vk_" + field_keys[k] + "']");

            elements.each(function () {
                //console.log($(this));
                if ($(this).is('img')) {                 
                    $(this).attr("src", block[field_keys[k]]);
                } else if (field_keys[k] == 'sex') {
                    if (block[field_keys[k]] == '2') {
                        $(this).html('М');
                    } else if (block[field_keys[k]] == '1') {
                        $(this).html('Ж');
                    }
                } else {
                    $(this).html(block[field_keys[k]]);
                }
            });
        }
    }
    
    if ($(".vk_load").length > 0) {
        $(".vk_load").show();
    }
}

function fillBlock(block) {
    uids = "";
    fields = {};
    addBlockToRequest(block);

    var script = document.createElement('SCRIPT');
    script.src = "https://api.vk.com/method/users.get?uids=" + uids + "&fields=" + Object.keys(fields).join() + "&callback=callbackFunc";
    document.getElementsByTagName("head")[0].appendChild(script);
}

function addBlockToRequest(block) {
    //console.log(uids);
    var classes = block.attr("class").split(" ");
    for (var vkClass in classes) {
        var classElem = classes[vkClass];
        if (classElem.substr(0, vkInfoPrefix.length) == vkInfoPrefix) {
            uids += classElem.substr(vkInfoPrefix.length) + ",";
        }
    }
    block.find("[class^=vk_]").each(function () {
        var classes = this.className.split(/\s+/);
        for (var i in classes) {
            if (classes[i].substring(0, 3) == 'vk_')
                fields[classes[i].substring(3)] = 1;
        }
    });
}

function vkRequest () {
    var blocks = $('[class^=' + vkInfoPrefix + ']');
    uids = "";
    blocks.each(function () {
        //uids = "";
        addBlockToRequest($(this));
        
    });
    console.log(uids);
    var script = document.createElement('SCRIPT');
    script.src = "https://api.vk.com/method/users.get?uids=" + uids + "&fields=" + Object.keys(fields).join() + "&callback=callbackFunc";
    document.getElementsByTagName("head")[0].appendChild(script);
}