﻿if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
              ? args[number]
              : match
            ;
        });
    };
}

function VkAuthentificate(getVkAccessCodeMethod, redirectUrl) {
    var encodeUrl = Base64.encode(redirectUrl);
    window.location = "https://oauth.vk.com/authorize?client_id={0}&scope={1}&redirect_uri={2}?redirectUrlBase64={3}&response_type=code".format("3712308", "", getVkAccessCodeMethod, encodeUrl);
}

$(function() {
    $("[rel='tooltip']").tooltip();
});