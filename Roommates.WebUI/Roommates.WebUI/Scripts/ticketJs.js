﻿function TicketJs(ticket) {
    this.ticketType = ticket.TicketTypes == 0 ? "Ищу квартиру" : "Сдаю квартиру";
    this.addressId = "addressId" + ticket.ID;
    this.details = ticket.Details == null ? "-" : ticket.Details;
    
    var sliderPrice = ticket.SliderPrice;
    var noSmoking = ticket.Smoking;
    var noPets = ticket.Pets;
    var vkId = ticket.VkID;
    var name = '<b class="vk_first_name"></b> <b class="vk_last_name"></b>';

    this.getTicketPrice = function(urlToRoubleIcon) {
        var ticketPrice = sliderPrice.PriceMin;
        if (ticket.TicketTypes == 0) {
            ticketPrice = "от " + ticket.SliderPrice.PriceMin + " до " + ticket.SliderPrice.PriceMax;
        }
        ticketPrice = '<img class="priceIcon" src="' + urlToRoubleIcon + '"/> ' + ticketPrice;
        return ticketPrice;
    };

    this.getNameInLink = function() {
        return '<a href="https://vk.com/id' + vkId + '" class="name">' + name + "</a>";
    };

    this.getPhotoInLink = function(size) {
        if (size != "")
            size = "_" + size;
        return '<a href="https://vk.com/id' + vkId + '"><img class="vk_photo' + size + '" src=""/></a>';
    };
}