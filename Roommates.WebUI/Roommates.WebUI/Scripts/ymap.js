﻿function dist(x0, x1, y0, y1) {
    return Math.pow((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0), 0.5);
}

function YandexMap(onloadFunction) {
    var place;
    var roommatesMap;
    var circle;
    var successFigureAddFunction;
    var searchControl;
    var mousedown;
    var prevRadius = 2000;
     cluster = "";
     placeMarks = [];

    this.getPlace = function () {
        return place;
    };

    this.getRoommatesMap = function() {
        return roommatesMap;
    };

    this.init = function () {
        place = window.ymaps.geolocation.city;
        roommatesMap = new ymaps.Map('map', {
            center: [window.ymaps.geolocation.latitude, window.ymaps.geolocation.longitude],
            zoom: 10
        });

        //var myToolbar = new ymaps.control.MapTools({
        //    items: ['drag', 'magnifier']
        //});

        roommatesMap.events.add('click', clickEventHandler);

        roommatesMap.behaviors.enable('scrollZoom');
        roommatesMap.behaviors.disable(['multiTouch']);
        roommatesMap.events.add(['mousedown', 'mouseup', 'mousemove', 'multitouchmove', 'multitouchstart'], function (e) {
            if (circle != null) {
                var coords = e.get('coordPosition');
                var distFromPoint = ymaps.coordSystem.geo.getDistance(coords, circle.geometry.getCoordinates());
                var radius = circle.geometry.getRadius();
                var isInSizeChangeArea = false;
                var cursor;
                //if(e.get('type')=='mousemove')
                //{
                if (distFromPoint >= radius * 0.9 && distFromPoint <= radius * 1.05) {
                    cursor = roommatesMap.cursors.push('move');
                    isInSizeChangeArea = true;
                    circle.options.set('draggable', false);
                    roommatesMap.behaviors.disable(['drag', 'rightMouseButtonMagnifier']);
                    //log.innerHTML = '@' + 'on the edge of the circle' + '<br/>' + log.innerHTML;
                }
                else if (distFromPoint < radius * 0.9) {
                    roommatesMap.cursors.push('grab');
                    //log.innerHTML = '@' + 'in the circle' + '<br/>' + log.innerHTML;
                    circle.options.set('draggable', true);
                    //roommatesMap.behaviors.enable(['drag', 'rightMouseButtonMagnifier']);
                }
                else {
                    //var touch=e.get('domEvent').get('clientX');
                    //log.innerHTML = '@' + touch + '<br/>' + log.innerHTML;
                    roommatesMap.cursors.push('grab');
                    roommatesMap.behaviors.enable(['drag', 'rightMouseButtonMagnifier']);
                }
                //}
                if (e.get('type') == 'mousedown' && isInSizeChangeArea) {
                    mousedown = true;
                }
                if (e.get('type') == 'mouseup') {
                    mousedown = false;
                }
                if (e.get('type') == 'multitouchmove') {
                    //log.innerHTML = '@' + e.originalEvent.target._Ab[0] + '<br/>' + log.innerHTML;
                    //log.innerHTML = '@' + e.originalEvent.target._Ab[1] + '<br/>' + log.innerHTML;
                    //for(var p in e)
                    //{
                    //	log.innerHTML = '@' + p + '<br/>' + log.innerHTML;
                    //}
                    //log.innerHTML = '@' + log.innerHTML;

                    //var touch=e.get('domEvent').callMethod('items')(0);
                    //log.innerHTML = '@' + touch.length + '<br/>' + log.innerHTML;
                }
                if (mousedown) {
                    //log.innerHTML = '@' + 'near edge' + '<br/>' + log.innerHTML;
                    prevRadius = ymaps.coordSystem.geo.getDistance(coords, circle.geometry.getCoordinates()) * 1.02;
                    circle.geometry.setRadius(prevRadius);
                }
            }
        });

        searchControl = new ymaps.control.SearchControl({ noPlacemark: true });
        searchControl.events.add('resultselect', selectSearchObject);

        roommatesMap.controls
            .add('zoomControl')
            .add(searchControl, { right: '10' });
        //.add(myToolbar);
        //.add('')
        //.add('mapTools');


        if (onloadFunction != null) {
            onloadFunction();
        }
    };

    var selectSearchObject = function (event) {
        var index = event.get('resultIndex');
        var result = searchControl.getResult(index);
        result.then(function (res) {
            var coord = res.geometry.getBounds()[0];
            currentFigureDrowByCoords(coord);
        });

    };

    this.initCluster = function() {
        cluster = new ymaps.Clusterer();
    };

    this.drawCluster = function() {
        cluster.options.set({
            gridSize: 200
        });
        
        roommatesMap.geoObjects.add(cluster);
    };
    this.getFrameCoords = function () {
        return roommatesMap.getBounds();
    };

    ymaps.ready(this.init);

    this.navigateTo = function (placeTo) {
        var myCollection = new ymaps.GeoObjectCollection();
        ymaps.geocode(placeTo, { results: 1 }).then(function (res) {
            myCollection.removeAll();
            myCollection = res.geoObjects;
            if (myCollection.getLength() == 1)
                myCollection.each(gotoGeoObject);
        });
        return false;
    };

    this.DecodeGeoObject = function (geoCoordinates, index, fillAddress) {
        var myCoords = geoCoordinates;//[55.754952, 37.615319];

        var geocoder = ymaps.geocode(myCoords);
        geocoder.then(
            function (res) {
                var nearest = res.geoObjects.get(0);
                var name = nearest.properties.get('name');
                fillAddress(name, index);
                //alert(name);
            },
            function (err) {
                //alert('Ошибка');
            }
        );
        //        alert("1");
    };

    var gotoGeoObject = function (geoObjext) {
        roommatesMap.panTo(geoObjext.geometry.getCoordinates(), {
            flying: true,
            duration: 3000
        });
    };

    this.clearOutsideTicket = function () {
        if (cluster != null)
            for (var i = 0; i < placeMarks.length; i++) {
                var curPlaceMark = placeMarks[i].placeMark;
                if (!curPlaceMark.balloon.isOpen()) {
                    cluster.remove(curPlaceMark);
                    placeMarks.splice(i--, 1);
                }
            }
    };

    this.clear = function() {
        roommatesMap.geoObjects.each(
            function (geoObject) {
                    roommatesMap.geoObjects.remove(geoObject);
            });
        placeMarks = [];
    };
    var clear = this.clear;

    this.addPlacemark = function (coords, content, ticketId) {
        for (var i = 0; i < placeMarks.length; i++) {
            if (placeMarks[i].ticketId == ticketId) {
                return null;
            }
        }

        var myPlacemark = new ymaps.Placemark(coords,
            {
                balloonContentHeader: content.header,
                balloonContentBody: content.body,
                balloonContentFooter: content.footer,
            },
            {
                preset: 'twirl#blueStretchyIcon',
            });
//        ymaps.geoQuery(placeMarks).addToMap(roommatesMap);
        cluster.add(myPlacemark);
        
        placeMarks.push({ placeMark: myPlacemark, ticketId: ticketId});
        return myPlacemark;
    };
    
    this.addPoint = function (coords, content) {
        roommatesMap.balloon.open(coords, {
            contentHeader: content.header,
            contentBody: content.body,
            contentFooter: content.footer
        });
    };
    var addPoint = this.addPoint;

    this.addCircle = function (coord, radius) {
        circle = new ymaps.Circle([
                coord,
                radius
        ], {
            //hintContent: "Где я хочу жить"
        }, {
            draggable: true,
            fillColor: "#DB709377",
            strokeColor: "#990066",
            strokeOpacity: 0.8,
            strokeWidth: 3
        });
        circle.events.add(['mouseenter', 'mouseleave', 'mousemove', 'mousedown', 'mouseup', 'multitouchmove', 'multitouchstart'],
            function (e) {
                var coords = e.get('coordPosition');
                var distFromPoint = ymaps.coordSystem.geo.getDistance(coords, circle.geometry.getCoordinates());
                var sizeChangeArea = circle.geometry.getRadius() * 0.9;
                var isInSizeChangeArea = false;
                if (distFromPoint >= sizeChangeArea) {
                    roommatesMap.cursors.push('move');
                    isInSizeChangeArea = true;
                    circle.options.set('draggable', false);
                    roommatesMap.behaviors.disable(['drag', 'rightMouseButtonMagnifier']);
                }
                else {
                    roommatesMap.cursors.push('grab');
                    isInSizeChangeArea = false;
                    circle.options.set('draggable', true);
                }
                if (e.get('type') == 'mousedown' && isInSizeChangeArea) {
                    mousedown = true;
                }
                if (e.get('type') == 'mouseup') {
                    mousedown = false;
                }
                if (e.get('type') == 'multitouchmove' || e.get('type') == 'multitouchstart') {
                    log.innerHTML = '@' + 'multitouch circle' + '<br/>' + log.innerHTML;
                }
                if (mousedown) {
                    //log.innerHTML = '@' + 'near edge' + '<br/>' + log.innerHTML;
                    prevRadius = ymaps.coordSystem.geo.getDistance(coords, circle.geometry.getCoordinates()) * 1.02;
                    circle.geometry.setRadius(prevRadius);
                }
            });
        roommatesMap.geoObjects.add(circle);
    };
    var addCircle = this.addCircle;

    var addCircleByCoords = function (coords) {
        clear();
        addCircle(coords, prevRadius);
        successFigureAddFunction({ coords: coords, radius: prevRadius });
    };

    var clickEventHandler = function (e) {
        var coords = e.get('coordPosition');
        if (currentFigureDrowByCoords != null) {
            currentFigureDrowByCoords(coords);
        }
    };

    var addPointByCoords = function (coords) {
        clear();

        var hoousePlacemark = new ymaps.Placemark(coords);

        var geocoder = ymaps.geocode(coords);
        geocoder.then(
            function (res) {
                var nearest = res.geoObjects.get(0);
                var balloonContent = nearest.properties.get('balloonContentBody');
                hoousePlacemark.properties.set('balloonContent', balloonContent);
            },
            function (err) {
            }
        );

        roommatesMap.geoObjects.add(hoousePlacemark);
        successFigureAddFunction({ coords: coords, radius: 0 });
    };

    var currentFigureDrowByCoords;

    this.setOnSuccessClick = function (successFunction) {
        successFigureAddFunction = successFunction;
    };

    this.setCurrentFigureCircle = function () {
        currentFigureDrowByCoords = addCircleByCoords;
    };

    this.setCurrentFigurePoint = function () {
        currentFigureDrowByCoords = addPointByCoords;
    };

    this.changeOnclickFigure = function () {
        if (currentFigureDrowByCoords == addPointByCoords) {
            this.setCurrentFigureCircle();
        } else {
            this.setCurrentFigurePoint();
        }
        this.clear();
    };

    this.getMap = function () {
        return roommatesMap;
    };

    this.onDrug = function (func) {
        roommatesMap.events.add('actionend', func);
    };
}