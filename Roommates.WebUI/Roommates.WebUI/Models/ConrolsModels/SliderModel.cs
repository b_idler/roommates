﻿using System;
using System.Reflection;
using Roommates.Common.Attributes;

namespace Roommates.WebUI.Models.ConrolsModels
{
    public class SliderModel
    {
        public int PriceMin { get; set; }

        public int PriceMax { get; set; } 
    }
}