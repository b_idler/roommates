﻿namespace Roommates.WebUI.Models
{
    public class PersonModel
    {
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string FirstName { get; set; }
    }
}
