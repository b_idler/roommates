﻿using System.ComponentModel.DataAnnotations;
using Roommates.Common.Attributes;
using Roommates.Common.Consts;
using Roommates.DataLayer.Model;
using Roommates.WebUI.Models.ConrolsModels;

namespace Roommates.WebUI.Models.TicketModels
{
    public class TicketModel
    {
        public int ID { get; set; }
        public double XCoord { get; set; }
        public double YCoord { get; set; }
        public double Radius { get; set; }

        public string VkID { get; set; }

        [Display(Name = "Цена за месяц")]
        public int? Price { get; set; }

        [Step(500)]
        [SliderRange(0, 30000)]
        public SliderModel SliderPrice { get; set; }

        [Display(Name = "Я")]
        public PersonSex Sex { get; set; }

        [Display(Name = "Пол соседей")]
        public Sex NeighbourSex { get; set; }

        [Display(Name = "Я негативно отношусь к курению")]
        public bool Smoking { get; set; }

        [Display(Name = "Я против содержания домашних животных")]
        public bool Pets { get; set; }

        [Display(Name = "В моей комнате")]
        public PersonCount PersonsCount { get; set; }

        [Display(Name = "У меня")]
        public TicketTypes TicketTypes { get; set; }

        [Display(Name = "Дополнительно")]
        public string Details { get; set; }

        public TicketModel()
        {
            SliderPrice = new SliderModel();
        }

        public TicketModel(Ticket ticket)
        {
            ID = ticket.ID;
            XCoord = ticket.XCoord;
            YCoord = ticket.YCoord;
            Radius = ticket.Radius;
            SliderPrice = new SliderModel
                {
                    PriceMin = ticket.PriceMin, 
                    PriceMax = ticket.PriceMax
                };
            Price = ticket.PriceMin;
            Sex = (PersonSex) (ticket.Sex);
            NeighbourSex = (Sex) ticket.NeighbourSex;
            Smoking = ticket.Smoking;
            Pets = ticket.Pets;
            PersonsCount = (PersonCount) ticket.PersonsCount;
            VkID = ticket.Person.VkID;
            TicketTypes = (TicketTypes) (ticket.WithHouse.HasValue ? (ticket.WithHouse.Value ? 1 : 0) : 0);
            Details = ticket.Details;
        }

        public void OnSetModel(Ticket ticket)
        {
            ticket.XCoord = XCoord;
            ticket.YCoord = YCoord;
            ticket.Radius = Radius;
            if (TicketTypes == TicketTypes.Appartment)
            {
                ticket.PriceMin = Price.Value;
                ticket.PriceMax = Price.Value;
            }
            else
            {
                ticket.PriceMin = SliderPrice.PriceMin;
                ticket.PriceMax = SliderPrice.PriceMax;   
            }
            ticket.Sex = (byte)Sex;
            ticket.NeighbourSex = (byte) NeighbourSex;
            ticket.Smoking = Smoking;
            ticket.Pets = Pets;
            ticket.PersonsCount = (int) PersonsCount;
            ticket.WithHouse = TicketTypes == TicketTypes.Appartment;
            ticket.Details = Details;
        }
    }
}