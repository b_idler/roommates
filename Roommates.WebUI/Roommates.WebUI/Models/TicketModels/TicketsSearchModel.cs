﻿using System.ComponentModel.DataAnnotations;
using Roommates.Common.Attributes;
using Roommates.Common.Consts;
using Roommates.DataLayer.Model;
using Roommates.WebUI.Models.ConrolsModels;
using Roommates.WebUI.Models.StandartModels;

namespace Roommates.WebUI.Models.TicketModels
{
    public class TicketsSearchModel
    {
        /// <summary>
        /// В дальнейшем для некоторых пользователей мы будем знать что-то о них
        /// </summary>
        public int? PersonId { get; set; }

        public PointDouble LeftUpCorner { get; set; }
        public PointDouble RightDownCorner { get; set; }

        [Step(500)]
        [SliderRange(0, 30000)]
        public SliderModel SliderModel { get; set; }

        [Display(Name = "Пол соседей")]
        public Sex NeighbourSex { get; set; }

        [Display(Name = "Я негативно отношусь к курению")]
        public bool Smoking { get; set; }

        [Display(Name = "Я против содержания домашних животных")]
        public bool Pets { get; set; }

        [Display(Name = "В моей комнате")]
        public PersonCount PersonsCount { get; set; }
        
        [Display(Name = "Ищу человека")]
        public TicketTypesSearch TicketTypes { get; set; }
    }
}