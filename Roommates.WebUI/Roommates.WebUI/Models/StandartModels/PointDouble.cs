﻿using System;

namespace Roommates.WebUI.Models.StandartModels
{
    public class PointDouble
    {
        public double X { get; set; }
        public double Y { get; set; }
        
        public PointDouble() { }

        public PointDouble(double x, double y)
        {
            X = x;
            Y = y;
        }
    }

    public static class PointDoubleExtension
    {
        public static double GetDistance(this PointDouble p1, PointDouble p2)
        {
            return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }
    }
}