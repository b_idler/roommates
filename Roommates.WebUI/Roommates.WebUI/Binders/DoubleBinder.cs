﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace Roommates.WebUI.Binders
{
    public class DoubleModelBinder : DefaultModelBinder
    {
        private const char Separator = '.';
        private readonly NumberFormatInfo _formatInfo;

        public DoubleModelBinder()
        {
            CultureInfo cultureInfo = CultureInfo.InvariantCulture;
            _formatInfo = (NumberFormatInfo)cultureInfo.NumberFormat.Clone();
            _formatInfo.NumberDecimalSeparator = Separator.ToString(CultureInfo.InvariantCulture);
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            ValueProviderResult valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (valueProviderResult == null)
            {
                return base.BindModel(controllerContext, bindingContext);
            }

            string valueStr = valueProviderResult.AttemptedValue;
            valueStr = valueStr.Replace(',', Separator);
            //valueStr = valueStr.Replace('.', Separator);

            try
            {
                return Double.Parse(valueStr, _formatInfo);
            }
            catch (Exception)
            {
                return base.BindModel(controllerContext, bindingContext);
            }
        }
    }
}