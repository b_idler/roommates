﻿using System;
using Roommates.Common.Consts;
using log4net;

namespace Roommates.Common.Services
{
    public interface IEventLogger
    {
        void Write(string msg, EventTypes? eventType = null, int? personId = null);
    }

    public class EventLogger : IEventLogger
    {
        private readonly ILog _logger;

        public EventLogger(ILog logger)
        {
            _logger = logger;
        }

        #region IEventLogger Members

        public void Write(string msg, EventTypes? eventType, int? personId)
        {
            if (String.IsNullOrWhiteSpace(msg))
                throw new ArgumentException("Параметр не может быть null или пустой строкой", "msg");

            string fullMessage = String.Format("Message: {0}\r\nUser: {1}\r\n", msg, personId);
            switch (eventType)
            {
                case EventTypes.Information:
                    _logger.Info(fullMessage);
                    break;
                case EventTypes.Warning:
                    _logger.Warn(fullMessage);
                    break;
                default:
                    _logger.Error(fullMessage);
                    break;
            }
        }

        #endregion
    }
}