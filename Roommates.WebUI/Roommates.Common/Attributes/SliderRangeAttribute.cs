﻿using System;

namespace Roommates.Common.Attributes
{
    public class SliderRangeAttribute : Attribute
    {
        public int MinValue { get; private set; }

        public int MaxValue { get; private set; }

        public SliderRangeAttribute(int minValue, int maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
        }
    }
}