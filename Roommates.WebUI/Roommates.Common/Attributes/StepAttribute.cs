﻿using System;

namespace Roommates.Common.Attributes
{
    public class StepAttribute : Attribute
    {
        public int StepValue { get; private set; }

        public StepAttribute(int i)
        {
            StepValue = i;
        }
    }
}