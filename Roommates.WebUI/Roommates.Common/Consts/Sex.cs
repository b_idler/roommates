﻿using System.ComponentModel;

namespace Roommates.Common.Consts
{
    public enum Sex
    {
        [Description("Женщина")]
        Female = 0,

        [Description("Мужчина")]
        Male = 1,

        [Description("Не имеет значения")]
        NotImportant = 2,
    }
}