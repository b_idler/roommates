﻿using System.ComponentModel;

namespace Roommates.Common.Consts
{
    public enum TicketTypesSearch
    {
        [Description("Без квартиры")]
        Human = 0,

        [Description("С квартирой")]
        Appartment = 1,

        [Description("Не имеет значения")]
        NotImportant = 2,
    }
}