﻿using System.ComponentModel;

namespace Roommates.Common.Consts
{
    public enum PersonCount
    {
        [Description("Могу жить только я")]
        Few = 0,

        [Description("Я могу жить не один")]
        Many = 1,
        
        [Description("Не имеет значения")]
        NotImportant = 2
    }
}