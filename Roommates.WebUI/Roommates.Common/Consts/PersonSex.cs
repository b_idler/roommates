﻿using System.ComponentModel;

namespace Roommates.Common.Consts
{
    public enum PersonSex
    {
        [Description("Женщина")]
        Female = 0,

        [Description("Мужчина")]
        Male = 1
    }
}