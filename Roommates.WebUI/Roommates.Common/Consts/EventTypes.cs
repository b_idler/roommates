﻿namespace Roommates.Common.Consts
{
    public enum EventTypes
    {
        Information = 1,
        Warning = 2,
        Error = 3,
        Critical = 4
    }
}
