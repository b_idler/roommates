﻿using System.ComponentModel;

namespace Roommates.Common.Consts
{
    public enum TicketTypes
    {
        [Description("Нет квартиры")]
        Human = 0,

        [Description("Есть квартира")]
        Appartment = 1
    }
}